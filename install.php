<?php
error_reporting(E_ALL);

define( '__APP_CONF', 'config/app.conf.php' );

$root_dif = explode( realpath( $_SERVER['DOCUMENT_ROOT'] ), __DIR__ );
$root_dif_split =  preg_split( '/\//', $root_dif[1], -1, PREG_SPLIT_NO_EMPTY );

function set_countent( $context ){
  return $context."\n";
}

$app_conf_content = set_countent( "<?php" );
$app_conf_content .=
set_countent( "define( '__APP_CONF', 'config/app.conf.php' );" );
$app_conf_content .=
  set_countent( "define( '__ROOT_DIR', '".
  realpath( $_SERVER['DOCUMENT_ROOT'] ) ."' );" );

if(!empty($root_dif[1])){
  $app_conf_content .=
    set_countent( "define( '__ROOT_DIF', '$root_dif[1]' );" );
  $app_conf_content .=
    set_countent( "define( '__ROOT_DIF_LENGTH', ".
    count( $root_dif_split ) ." );" );
}

$app_conf_content .= set_countent("?>");

file_put_contents(__APP_CONF, $app_conf_content);
?>
